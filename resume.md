Fabio Utzig
===========

<http://github.com/utzig>
<http://fabioutzig.com>
<http://linkedin.com/in/utzig>

## Profile

I have been writing software for 15 years, where most of this time was spent
writing systems software for Linux based systems using C/C++. The last 7 years I
consulted for many companies writing embedded systems software for all kinds of
micro-controllers and embedded Linux based systems, I wrote financial applications and
currently I write application software in Python using PyQt. I love unix based
systems, writing low-level software, modern systems programming languages like Go and
Rust and dynamic languages like Python and Ruby.

## Skills

- - C (_expert_), Python (_advanced_), C++ (_intermediate_), Ruby (_intermediate_),
    Go (_intermediate_), shell script (_intermediate_).
- - Experienced user/programmer of POSIX OSes (mostly Linux and BSDs).
- - Good knowledge of hardware, firmware development, low-level and baremetal programming.
- - Experienced with network programming (sockets) and concurrent software development.

## Work Experience

*   **ESSS** (Florianópolis, Brazil)

    Python software developer, March 2015 - Present

    I work on an oil drilling application where I am responsible to write the layer
    that gathers data from the drilling machinery sensors and by interpreting this
    data and applying various real-time physical calculations, alerts users if any
    failures are predicted to happen. I write mostly Python code.

*   **Perto** (Gravataí, Brazil)

    Software developer, August 2013 - June 2014

    I was part of a team creating a Point Of Sale machine.

    -   I developed of a financial application for the largest state bank in
        RS/Brazil. It runs on special POS hardware, processes transactions with
        CHIP/magnetic cards and communicates through GPRS with the bank's server. I
        became the main developer after some months; was also responsible for
        mentoring other developers.

    -   Worked on the proprietary libraries (layered over Linux standard libraries)
        where I coded fixes and improvements to font rendering and support for
        playing audio/videos by integrating ffmpeg. Wrote mostly C code.

*   **Freelancer**

    June 2009 - August 2013

    I contracted for many companies. My responsibilities were usually to develop
    a complete software application or firmware from the ground up. For the
    embedded products I also had key participation in hardware designs to better
    define the most apropriate options to solve a given problem.

    _PS: more details can be given on request_.

    _keywords_: ChibiOS/RT, ThreadX, XBee, FLTK, Qt, Qwt, C++, UDP, sockets,
    STM32, GSM, RFID, Over-the-Air Firmware Upgrade, Arduino, AVR, ARM, STM32,
    iMX233, Davinci.

*   **Altus Sistemas de Informática** (São Leopoldo, Brazil)

    Programmer, June 2008 - June 2009

    -   Part of the team developing a PLC for automation of energy stations
        running an IEC 870-5-104 slave stack. My main responsibility was
        developing _commands_ (outputs) and a _bridge_ to convert commands
        received from a DNP master.

    -   Responsible for the  _bringup_ another PLC series running _U-Boot_/Linux.
        I also implemented some initial _drivers_ for this equipment supporting
        some I2C/SPI/GPIO peripherals.

*   **T&T Engenheiros Associados** (Porto Alegre, Brazil)

    Programmer, July 2007 - May 2008

    -   I was the technical leader in a project to update firmware for the
        administrative module on the HP Bladesystem c7000 and c3000.
        The purpose of the project was enabling blades to be composed of
        dual machines, one per bay (HP BL2x200c G5). The product was based on
        embedded Linux running on a PowerPC processor.

*   **Datacom Telemática** (Porto Alegre, Brazil)

    Programmer, March 2007 - July 2007

    -   Did bug fixing and small improvements to a specialized and proprietary
        network routing layer inside the Linux kernel.

*   **Elipse Software** (Porto Alegre, Brazil)

    Programmer, July 2006 - March 2007

    -   I was a C++ developer on a SCADA system. My main contributions
        were adding compression to the communication protocol and
        writing a bridge layer to communicate with other SCADA protocols.

*   **T&T Engenheiros Associados** (Porto Alegre, Brazil)

    Programmer, December 2001 - July 2006

    -   Worked as a contractor for HP Brasil. Some of the most significant
        projects I worked on were _HP TopTools_, a Net-SNMP based product tailored
        for HP's servers, a test and diagnostic suite for Compaq, an image
        server for fast server OS's installation. The software I wrote was mostly
        coded in C/C++/Perl and run on Linux servers.

*   **Altus Sistemas de Informática** (Porto Alegre, Brazil)

    Intern, June 2001 - December 2001

    -   I was the maintainer of an IEC 870-5-101 protocol stack running over an
        RTOS inside a PLC.

<!--

*   **Perto** (Gravataí, Brazil)

    Intern, January 2001 - June 2001

    -   I was an intern in the quality assurance department where I wrote an application
        software for testing Point of Sale terminals from the groundup in Labview.

*   **ZYX Computadores do Brasil** (São Leopoldo, Brazil)

    Technician, August 1997 - September 2000

    -   Responsible for hw maintenance and hw/sw networking infrastructure.
    -   Developed an 8051 based control system for automation of radio stations.
        I designed, layouted using Tango and wrote firmware in assembly. This was
        a niche product that sold hundreds of units.
-->

## Open Source

*   <http://chibios.org> - Contributor and maintainer of AVR, Freescale's Kinetis
    and Nordic's nRF51x ports.

*   <http://github.com/utzig/lm4tools> - Flasher for TI's line of Stellaris/Tiva Launchpad
    boards.

## Education

*   **UNISINOS** (São Leopoldo, Brazil)

    B.S. in Computer Science, June 2010
